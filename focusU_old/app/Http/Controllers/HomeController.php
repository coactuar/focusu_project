<?php
   
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Models\Question;  
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $where = array('active' => '1');
        $questions  = Question::where($where)->get();
        // print_r($questions);
        return view("home", ['questions'=>$questions]);

    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
        return view('adminHome');
    }
    
}