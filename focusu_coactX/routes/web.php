<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use Illuminate\Http\Request; 
use App\Http\Controllers\AnswertableController;
use App\Http\Controllers\StatusController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
// Route::post('/test', function (Request $request) {
    
//    print_r($request->all());
// })->name('test');
Route::post('/test', [App\Http\Controllers\AdminController::class, 'testresult'])->name('test');
 Route::post('/admindata', [App\Http\Controllers\AdminController::class, 'index'])->name('admindata');
 Route::get('/scoredata/{round}/{userid}', [App\Http\Controllers\AnswertableController::class, 'score'])->name('scoredata');
 Route::get('score-card', [AnswertableController::class, 'index']);
 Route::get('score-export', [AnswertableController::class, 'export'])->name('score-export');
 Route::post('/update', [App\Http\Controllers\AdminController::class, 'updatescore'])->name('update');
//  Route::post('store-company', [AnswertableController::class, 'store']);
//  Route::post('edit-company', [AnswertableController::class, 'edit']);
//  Route::post('delete-company', [AnswertableController::class, 'destroy']);
Route::get('rounds', [StatusController::class, 'index']);
Route::post('downloaddata', [AdminController::class, 'downloaddata'])->name('downloaddata');
Route::post('change-status', [StatusController::class, 'changeStatus']);

 Route::get('admin/dashboard', function () {
    return view('dashboard');
    
})->middleware('is_admin');
