<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Models\Status;  
use App\Models\Question;   
class StatusController extends Controller
{

    public function index()
    {
        $users = Status::get();
        return view('status',compact('users'));
    }

    public function changeStatus(Request $request)
    {
        
        $user = Status::find($request->id)->update(['status' => $request->status]);
        
        $question = Question::where('round', $request->round)->update(['active' =>$request->status]);
        
         return response()->json(['Updated']);
    }
}