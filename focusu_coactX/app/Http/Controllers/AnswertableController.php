<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Answers;
use App\Models\Scoretable;
use App\Models\Downloaddata;
use App\Models\Question;
use Illuminate\Support\Facades\DB;
use Datatables;
use App\Exports\Export;
use Maatwebsite\Excel\Facades\Excel;
class AnswertableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data=Downloaddata::select('datadownload')->first();

        if(request()->ajax()) {
            $answers = Scoretable::join('users', 'scoretables.userid', '=', 'users.id')
            // ->join('questions', 'scoretables.qid', '=', 'scoretables.id')
            ->select(['scoretables.userid','users.name', 'users.email','scoretables.round1', 'scoretables.round2','scoretables.round3']);
    // //    return $answers;

            return datatables()->of($answers)
            // ->addColumn('action', 'company-action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        if($data)
            $data=$data->datadownload;
        else
            $data=null;
        return view('answertable',compact('data'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $companyId = $request->id;

        $company   =   Answers::updateOrCreate(
                    [
                     'id' => $companyId
                    ],
                    [
                    'name' => $request->name,
                    'email' => $request->email,
                    'address' => $request->address
                    ]);

        return Response()->json($company);

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $where = array('id' => $request->id);
        $company  = Answers::where($where)->first();

        return Response()->json($company);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answers  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $company = Answers::where('id',$request->id)->delete();

        return Response()->json($company);
    }
    public function score(Request $request)
    {

        $questions = Answers::join('users', 'answers.userid', '=', 'users.id')
             ->join('questions', 'answers.qid', '=', 'questions.id')->where('round',$request->round)
             ->where('answers.userid',$request->userid)->select('*')->get();
        //  print_r($questions); die();
        // $company = Answers::where('id',$request->id)->delete();
        return view("answerpaper", ['questions'=>$questions]);
        // return Response()->json($company);
    }
    public function export()
    {
        return Excel::download(new Export, 'Scores.xlsx');
    }

}
