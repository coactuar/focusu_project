<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 
        'topic',  
        'date', 
        'question',
        'q_type',
        'opt1',
        'opt2',
        'opt3',
        'opt4',
        'opt5',
        'opt6',
        'opt7',
        'opt8',
        'curans',
        'link',
        'round',
        'active',
        'point'
    ];
}
