<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Scoretable extends Model
{
    use HasFactory;
    protected $fillable = [
        'userid', 
        'round1',  
        'round2', 
        'round3',
        
         ];
}
