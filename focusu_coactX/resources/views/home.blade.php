@extends('layouts.app')

@section('content')

<style>
      body{
      background-image: url("{{url('images/orange.jpg')}}" );
      font-family: "Baloo Paaji", cursive;
    }
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">

                <div class="progress">
                <div id="upload" class="progress-bar"></div>
                </div>
                <div id="uploadStatus"></div>
                <form method="POST"  id="uploadform" action="{{ route('test') }}" enctype="multipart/form-data">
                @csrf
                @if(sizeof($questions)!=0)
                <div class="card-header"><h1> {{$questions[0]['round']}}</h1></div>

                    @foreach($questions as $question)


                    @switch($question->q_type)
                        @case('file')
                        <h1>{{$question->question}}</h1>
                        {{$question->id.Auth::user()->name}}
                        <!-- <input type="hidden" value="{{Auth::user()->name}}"  name="linkname"/> -->
                        <input type="hidden" value="{{Auth::user()->name}}" name="{{$question->id}}" id="link" />
                        <!-- <input type="hidden"  name="linkname" id="linkname" name="{{$question->id}}"/> -->

                <label>Choose File:</label>

                    <input type="file" name="file{{$question->id}}"   id="input" >

                    <!-- <input type="hidden"  name="linkname" id="linkname" value=""/> -->


                    @break

                        @case('text')
                        <h1>{{$question->question}}</h1>
                        <!-- <input type="hidden"  name="linkname" id="linkname" name="{{$question->id}}"/> -->
                        <p><input type="text" name="{{$question->id}}"></p>
                         @break
                         @case('polls')
                        <h1>{{$question->question}}</h1>
                     @if(is_null($question->opt1))<p> <input type="hidden" ></p>@else <p><input type="radio" name="{{$question->id}}" value="opt1">{{$question->opt1}} </p> @endif
                     @if(is_null($question->opt2))<p> <input type="hidden" ></p>@else <p><input type="radio" name="{{$question->id}}" value="opt2">{{$question->opt2}}</p> @endif
                     @if(is_null($question->opt3))<p> <input type="hidden" ></p>@else <p><input type="radio" name="{{$question->id}}"  value="opt3">{{$question->opt3}}</p> @endif
                     @if(is_null($question->opt4))<p> <input type="hidden" ></p>@else <p><input type="radio" name="{{$question->id}}"  value="opt4">{{$question->opt4}}</p> @endif
                     @if(is_null($question->opt5))<p> <input type="hidden" ></p>@else <p><input type="radio" name="{{$question->id}}"   value="opt5">{{$question->opt5}}</p> @endif
                     @if(is_null($question->opt6))<p> <input type="hidden" ></p>@else <p><input type="radio" name="{{$question->id}}"  value="opt6">{{$question->opt6}}</p> @endif
                     @if(is_null($question->opt7))<p> <input type="hidden" ></p>@else <p><input type="radio" name="{{$question->id}}"  value="opt7">{{$question->opt7}}</p> @endif
                     @if(is_null($question->opt8))<p> <input type="hidden" ></p>@else <p><input type="radio" name="{{$question->id}}"  value="opt8">{{$question->opt8}}</p> @endif

                         @break
                         @case('link')
                        <h1>{{$question->question}}</h1>
                        {{$url = "https://coact-stream-bucket.s3.ap-southeast-2.amazonaws.com/" . $question->link;}}

                        <video width="320" height="240" poster="{{$url}}" controls>
                        <source src = "{{$url}}" type="video/mp4" >

  Your browser does not support the video tag.
</video><p><input type="text" name="{{$question->id}}"></p>
                         @break
                     @default
                        Default case...
                    @endswitch
                    @endforeach


                    <input type="submit" value="Submit">
                     @endif

                    </form>
                    You are normal user.
                </div>
            </div>
        </div>
    </div>
</div>

  @endsection
  @push('scripts')
  <script>
      function myFunction(){
        alert("hii")
    }
$(document).ready(function(){
    // File upload via Ajax
    $(document).on('change', 'input', function(e) {
        fileType= e.target.files[0].type;
    fileName= e.target.files[0].name;
   alert(fileType)
    link=document.getElementById("link").value;
    // userid=document.getElementById('input').name;
    // link.not(this).remove();


    file= e.target.files[0];
    filename=link+fileName;
    // $("linkname").val(filename);
    alert(filename)

//     $(document).on('input', '#input', function() {

// });





    alert("high")
    var _URL = window.URL || window.webkitURL;

    // var file, img;


  //  alert("high")
//     fileType=document.getElementById('filecheck').files[0].type;
//     fileName=document.getElementById('filecheck').files[0].name;
//     link=document.getElementById('link').value;

//     filename=link+fileName;
//     $("linkname").val(filename);
//     alert(filename)
// fileName=filename;
if(true){

let data=   JSON.stringify({ "fileName": filename, "contentType": fileType });
$.ajax({


        type: 'POST',
        url:  "https://cj9k4nzvse.execute-api.ap-southeast-2.amazonaws.com/dev/fileUploadService",
        data:data ,
        error: function(error) {
        //  alert(JSON.stringify(error));
        },
        success: function(resp) {

        var posterurl=resp.uploadURL;
    // alert(posterurl)



var formData = new FormData();
formData.append('file',file);
// alert(JSON.stringify(formData))
// alert(fil.width )
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        $(".progress-bar").width(percentComplete + '%');
                        $(".progress-bar").html(percentComplete+'%');
                    }
                }, false);
                return xhr;
            },
            url:posterurl,
            type: 'PUT',
            data: file,
            beforeSend: function () { // Before we send the request, remove the .hidden class from the spinner and default to inline-block.
                $(".progress-bar").width('0%');
                $('#uploadStatus').html('<img src="public/images/download.png"/>');
        },
            contentType: false,
            processData: false,
            cache: false,
            error: function (data) { /*alert(data);*/
            },
            success: function (response) {
                // alert(JSON.stringify(response))





            },
            complete: function () { // Set our complete callback, adding the .hidden class and hiding the spinner.
                $('#uploadStatus').html('<p style="color:#28A74B;">File has uploaded successfully!</p>');
                    $('#uploadStatus').addClass('hidden')
          // location.reload();
        },
        });


         }
    });

}else{

alert("Height and width should be 1170*540")
}
/* alert(this.files[0]); */




});
});

</script>
    @endpush
