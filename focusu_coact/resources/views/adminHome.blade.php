@extends('layouts.app')
   
@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    You are Admin.
                </div>
            </div>
        </div>
    </div>
</div> -->
<html>

<head>
  <meta charset="utf-8">
  <title>focusU</title>
  <base target="_self">
  <meta name="description" content="A Bootstrap 4 admin dashboard theme that will get you started. The sidebar toggles off-canvas on smaller screens. This example also include large stat blocks, modal and cards. The top navbar is controlled by a separate hamburger toggle button."
  />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="google" value="notranslate">
  <link rel="shortcut icon" href="/images/cp_ico.png">


  <!--stylesheets / link tags loaded here-->


  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />


  <style type="text/css">
    body,
    html {
      height: 100%;
    } 

    
    body.modal-open {
      padding-right: 0 !important;
    }
    

    #bg{
      background-image: url("{{url('images/orange.jpg')}}" );

    }
    #sidebar {
      padding-left: 0;
    }

    
    @media screen and (max-width: 48em) {
      .row-offcanvas {
        position: relative;
        -webkit-transition: all 0.25s ease-out;
        -moz-transition: all 0.25s ease-out;
        transition: all 0.25s ease-out;
      }
      .row-offcanvas-left .sidebar-offcanvas {
        left: -33%;
      }
      .row-offcanvas-left.active {
        left: 33%;
        margin-left: -6px;
      }
      .sidebar-offcanvas {
        position: absolute;
        top: 0;
        width: 33%;
        height: 100%;
      }
    }

    
    @media screen and (max-width: 34em) {
      .row-offcanvas-left .sidebar-offcanvas {
        left: -45%;
      }
      .row-offcanvas-left.active {
        left: 45%;
        margin-left: -6px;
      }
      .sidebar-offcanvas {
        width: 45%;
      }
    }
    
   
  </style>

</head>

<body  id="bg">
  <!-- <nav class="navbar navbar-fixed-top navbar-toggleable-sm navbar-inverse bg-primary mb-3">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#collapsingNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="flex-row d-flex">
      <a class="navbar-brand mb-1" href="#">Brand</a>
      <button type="button" class="hidden-md-up navbar-toggler" data-toggle="offcanvas" title="Toggle responsive left sidebar">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse" id="collapsingNavbar">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">Home</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#features">Features</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#myAlert" data-toggle="collapse">Wow</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="" data-target="#myModal" data-toggle="modal">About</a>
        </li>
      </ul>
    </div>
  </nav> -->
  <form method="POST"  id="uploadform" action="{{ route('admindata') }}" enctype="multipart/form-data">
  @csrf
  <div class="container-fluid" id="main" >
    <div class="row row-offcanvas row-offcanvas-left">
      <div class="col-md-3 col-lg-2 sidebar-offcanvas" id="sidebar" role="navigation">
        <!-- <ul class="nav flex-column pl-1">
          <li class="nav-item"><a class="nav-link" href="#">Overview</a></li>
          <li class="nav-item">
            <a class="nav-link" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Reports ▾</a>
            <ul class="list-unstyled flex-column pl-3 collapse" id="submenu1" aria-expanded="false">
              <li class="nav-item"><a class="nav-link" href="">Sub item</a></li>
              <li class="nav-item"><a class="nav-link" href="">Sub item</a></li>
            </ul>
          </li> -->
          <!-- <li class="nav-item"><a class="nav-link" href="#">Analytics</a></li>
          <li class="nav-item"><a class="nav-link" href="#">Export</a></li>
          <li class="nav-item"><a class="nav-link" href="">Nav item</a></li>
          <li class="nav-item"><a class="nav-link" href="">Nav item</a></li>
          <li class="nav-item"><a class="nav-link" href="">Another</a></li>
          <li class="nav-item"><a class="nav-link" href="">Nav item</a></li>
          <li class="nav-item"><a class="nav-link" href="">One more</a></li> -->
        </ul>
      </div>

      <section>
    <div class="container">
      
    
      
      <div class="row justify-content-center">
        
        <div class="col-12 col-md-8 col-lg-8 col-xl-6">
          <div class="row">
            <div class="col text-center">
     
              <h1 class="text-white">Create Campain</h1>
              <!-- <p class="text-h3">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia. </p> -->
            </div>
          </div>
         
          <div class="row align-items-center">
          <div class="col mt-4">
              <!-- <input type="text" class="form-control" name="name" placeholder="Event Name"> -->
            </div>
            <div class="col mt-4">
              <!-- <input type="text" class="form-control" name="topic" placeholder="Topic"> -->
            </div>
          </div>
          <div class="row align-items-center mt-4">
            <div class="col">
              <!-- <input type="date" name="date" class="form-control" > -->
            </div>
          </div>
          <div class="row align-items-center mt-4">
            <div class="col">
              <!-- <input type="time"  name="time" class="form-control" > -->
            </div>
          
          </div>
          <div class="row align-items-center">
            <div class="col mt-4">
              <input type="text" name="question" class="form-control" placeholder="Enter Your Question">
            </div>
          </div>
          <div class="row align-items-center mt-4">
            <div class="col">
            <select id="size_select" name="q_type" class="form-control">
  <option value="option1" >Select any Question</option>
  <option value="option2" >Multiple Choose Question</option>
  <!-- <option value="option3">Choose Checkbox Question</option> -->
  <option value="option4"  >Choose Video/Audio/Image Question</option>
  <!-- <option value="option5" >Choose Video Question</option>
  <option value="option6"  >Choose Image Question</option> -->
  <option value="option6">Upload Video From User </option> 
  <option value="option7">Choose Text Question</option> 
</select>

<!-- <div id="option1" class="size_chart">
 <form id="form1">This is form 1.</form>
</div> -->


<div id="option2" class="size_chart">

 <div class="container" id="form1">
      
    
      
      <div class="row justify-content-center">
        <div class="col-12 ">
 
          <!-- <div class="row align-items-center">
            <div class="col mt-4">
              <input type="text" name="question" class="form-control" placeholder="Enter Your Question">
            </div>
          </div> -->
          <div class=" mt-4">
          <select id="size_select_options" name="choose" class="form-control">
            <option value="">Select Options</option>
  <option value="choose1"  name="choose1">option 4</option>
  <option value="choose2"  name="choose2">option 6</option>
  <option value="choose3"  name="choose3">option 8</option>

</select>

<section id="options4">
<div id="choose1" class="size_chart_options">
<div class="row align-items-center mt-4" id="form1">          
<div class="col">
 <input type="text" class="form-control" placeholder="Option 1" id="choosee41" name="opt1">
</div>
<div class="col">
 <input type="text" class="form-control" placeholder="Option 2"  id="choosee42" name="opt2">
</div>
</div>
<div class="row align-items-center mt-4">
<div class="col">
 <input type="text" class="form-control" placeholder="Option 3" id="choosee43" name="opt3">
</div>
<div class="col">
 <input type="text" class="form-control" placeholder="Option 4" id="choosee44" name="opt4" >
</div>

</div>
<div class="row align-items-center mt-4">
          <div class="col">
            <p class="text-white">Choose Correct Answer</p>
            <select id="size_select" name="curans" class="form-control">
  <option value="opt1" name="1"> 1</option>
  <option value="opt2"  name="2"> 2</option>
  <option value="opt3"  name="3"> 3</option>
  <option value="opt4"  name="3"> 4</option>
</select>
            </div>
          </div>
</div>

</section>

<section id="options6" >
<div id="choose2" class="size_chart_options" class="d-none">


<div class="row align-items-center mt-4" id="form1">

<div class="col">
  <input type="text" class="form-control" placeholder="Option 1" id="choosee61" name="opt1">
</div>
<div class="col">
  <input type="text" class="form-control" placeholder="Option 2" id="choosee62" name="opt2">
</div>
</div>
<div class="row align-items-center mt-4">
<div class="col">
  <input type="text" class="form-control" placeholder="Option 3" id="choosee63" name="opt3" >
</div>
<div class="col">
  <input type="text" class="form-control" placeholder="Option 4" id="choosee64" name="opt4">
</div>
</div>
<div class="row align-items-center mt-4">
<div class="col">
  <input type="text" class="form-control" placeholder="Option 5" id="choosee65" name="opt5" >
</div>
<div class="col">
  <input type="text" class="form-control" placeholder="Option 6"  id="choosee66" name="opt6" >
</div>
<!-- <div class="row align-items-center mt-4">
          <div class="col">
            <p class="text-white">Choose Correct Answer</p>
            <select id="size_select" name="curans" class="form-control">
  <option value="1" name="1"> 1</option>
  <option value="2"  name="2"> 2</option>
  <option value="3"  name="3"> 3</option>
  <option value="4"  name="4"> 4</option>
  <option value="5" name="5"> 5</option>
  <option value="6"  name="6"> 6</option>
</select>
            </div>
          </div> -->
          
</div>
<div class="row align-items-center mt-4">
<div class="col">
            <p class="text-white">Choose Correct Answer</p>
            <select id="size_select" name="curans" class="form-control">
  <option value="opt1" name="1"> 1</option>
  <option value="opt2"  name="2"> 2</option>
  <option value="opt3"  name="3"> 3</option>
  <option value="opt4"  name="4"> 4</option>
  <option value="opt5" name="5"> 5</option>
  <option value="opt6"  name="6"> 6</option>
</select>
            </div>
</div>
</div>
</section> 

 <section id="options8" class="">
<div id="choose3" class="size_chart_options">

<div class="row align-items-center mt-4" id="form1">

          
<div class="col">
  <input type="text" class="form-control" placeholder="Option 1" id="choosee81" name="opt1">
</div>
<div class="col">
  <input type="text" class="form-control" placeholder="Option 2" id="choosee82" name="opt2">
</div>

</div>
<div class="row align-items-center mt-4">
<div class="col">
  <input type="text" class="form-control" placeholder="Option 3"  id="choosee83" name="opt3">
</div>
<div class="col">
  <input type="text" class="form-control" placeholder="Option 4" id="choosee84" name="opt4">
</div>


</div>
<div class="row align-items-center mt-4">
<div class="col">
  <input type="text" class="form-control" placeholder="Option 5" id="choosee85" name="opt5" >
</div>
<div class="col">
  <input type="text" class="form-control" placeholder="Option 6" id="choosee86" name="opt6" >
</div>



</div>
<div class="row align-items-center mt-4">

<div class="col">
  <input type="text" class="form-control" placeholder="Option 7" id="choosee87" name="opt7">
</div>
<div class="col">
  <input type="text" class="form-control" placeholder="Option 8" id="choosee88" name="opt8" >
</div>
</div>

   
            <div class="row align-items-center mt-4">
          <div class="col">
            <p class="text-white">Choose Correct Answer</p>
            <select id="size_select" name="curans" class="form-control">
  <option value="opt1" name="1"> 1</option>
  <option value="opt2"  name="2"> 2</option>
  <option value="opt3"  name="3"> 3</option>
  <option value="opt4"  name="4"> 4</option>
  <option value="opt5" name="5"> 5</option>
  <option value="opt6"  name="6"> 6</option>
  <option value="opt7"  name="7"> 7</option>
  <option value="opt8"  name="8"> 8</option>
</select>
            </div>
          </div>



</div>
         
</section>
  </div>

          
        
        </div></div>
 </div>


</div>
<div id="option3" class="size_chart" id="form1">
 
 
 

</div>
<div id="option4" class="size_chart" >

  <div class="container" id="form1">
      
    
      
      <div class="row justify-content-center">
        <div class="col-12 ">
 
          <!-- <div class="row align-items-center">
            <div class="col mt-4">
              <input type="text" class="form-control"  placeholder="Enter Your Question">
            </div>
     
          </div> -->
          <div class="progress">
                <div id="upload" class="progress-bar"></div>
                </div>
                <div id="uploadStatus"></div>
          <div class="row align-items-center">
         
            <div class="col mt-4">
              <input type="file" class="form-control" name="link" id="input"  placeholder="Enter Your Question">
            </div>
     
          </div>
        </div></div></div>

</div>
<!-- <div id="option5" class="size_chart">

<div class="container" id="form1">
      
    
      
      <div class="row justify-content-center">
        <div class="col-12 ">
 
          <div class="row align-items-center">
            <div class="col mt-4">
              <input type="text" class="form-control" placeholder="Enter Your Question">
            </div>
     
          </div>
          <div class="row align-items-center">
            <div class="col mt-4">
              <input type="file" class="form-control" placeholder="Enter Your Question">
            </div>
     
          </div>
        </div></div></div>  

</div>
<div id="option6" class="size_chart">

<div class="container" id="form1">
     
      <div class="row justify-content-center">
        <div class="col-12 ">
 
          <div class="row align-items-center">
            <div class="col mt-4">
              <input type="text" class="form-control" placeholder="Enter Your Question">
            </div>
     
          </div>
          <div class="row align-items-center">
            <div class="col mt-4">
              <input type="file" class="form-control" name="file" placeholder="Enter Your Question">
            </div>
     
          </div>
        </div></div></div>

</div> -->
<div id="option7" class="size_chart">


<div class="container" id="form1">
      
    
      
      <div class="row justify-content-center">
        <div class="col-12 ">
 
          <!-- <div class="row align-items-center" name="text_ques">
            <div class="col mt-4">
              <input type="text" class="form-control" name="text_ques" placeholder="Enter Your Question">
            </div>
     
          </div> -->
          <div class="row align-items-center">
            <!-- <div class="col mt-4">
              <input type="text" class="form-control" placeholder="Enter Your Answer">
            </div> -->
     
          </div>
        </div></div></div>

</div>
            </div>
   
          </div>
          <div class="row align-items-center mt-4">
            <div class="col">
              <input type="number" class="form-control" name="score" placeholder="Score" >
            </div>
          
          </div>
          <div class="row align-items-center mt-4">
          <div class="col">
            <select id="size_select" name="round" class="form-control">
  <option value="round1" name="round1">Round 1</option>
  <option value="round2"  name="round2">Round 2</option>
  <option value="round3"  name="round3">Round 3</option>

</select>
            </div>
          </div>
          <div class="row justify-content-start">
            <div class="col">
    

              <button class="btn btn-primary mt-4">Submit</button>
            </div>
          </div>
        </div>
      </div>
    </div>
   
  </section>
  <form >




  <script>

    // sandbox disable popups
    if (window.self !== window.top && window.name != "view1") {
      window.alert = function() {
        /*disable alert*/
      };
      window.confirm = function() {
        /*disable confirm*/
      };
      window.prompt = function() {
        /*disable prompt*/
      };
      window.open = function() {
        /*disable open*/
      };
    }
    
    // prevent href=# click jump
    document.addEventListener(
      "DOMContentLoaded",
      function() {
        var links = document.getElementsByTagName("A");
        for (var i = 0; i < links.length; i++) {
          if (links[i].href.indexOf("#") != -1) {
            links[i].addEventListener("click", function(e) {
              console.debug("prevent href=# click");
              if (this.hash) {
                if (this.hash == "#") {
                  e.preventDefault();
                  return false;
                } else {
                  /*
                      var el = document.getElementById(this.hash.replace(/#/, ""));
                      if (el) {
                        el.scrollIntoView(true);
                      }
                      */
                }
              }
              return false;
            });
          }
        }
      },
      false
    );
  </script>

  <!--scripts loaded here-->

  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>

  <script>
    $(document).ready(function() {
      $("[data-toggle=offcanvas]").click(function() {
        $(".row-offcanvas").toggleClass("active");
      });

      $(".size_chart").hide();


// $("#option1").show();


$("#size_select").change(function(){

  $('.size_chart').hide();

  $('#'+$(this).val()).show();
});


$(".size_chart_options").hide();
// $("#option1").show();
$("#size_select_options").change(function(){

  $('.size_chart_options').hide();

  $('#'+$(this).val()).show();
  if($(this).val()=="choose1"){
    $('#choosee41').attr('name', 'opt1');
    $('#choosee42').attr('name', 'opt2');
    $('#choosee43').attr('name', 'opt3');
    $('#choosee44').attr('name', 'opt4');
    $('#choosee81').removeAttr("name");
    $('#choosee82').removeAttr("name");
    $('#choosee83').removeAttr("name");
    $('#choosee84').removeAttr("name");
    
    $('#choosee61').removeAttr("name");
    $('#choosee62').removeAttr("name");
    $('#choosee63').removeAttr("name");
    $('#choosee64').removeAttr("name");
   
// alert("hi")
  }else if($(this).val()=="choose2"){
    $('#choosee61').attr('name', 'opt1');
    $('#choosee62').attr('name', 'opt2');
    $('#choosee63').attr('name', 'opt3');
    $('#choosee64').attr('name', 'opt4');
    $('#choosee65').attr('name', 'opt5');
    $('#choosee66').attr('name', 'opt6');
    $('#choosee81').removeAttr("name");
    $('#choosee82').removeAttr("name");
    $('#choosee83').removeAttr("name");
    $('#choosee84').removeAttr("name");
    $('#choosee85').removeAttr("name");
    $('#choosee86').removeAttr("name");
    $('#choosee87').removeAttr("name");
    $('#choosee88').removeAttr("name");
    $('#choosee41').removeAttr("name");
    $('#choosee42').removeAttr("name");
    $('#choosee43').removeAttr("name");
    $('#choosee44').removeAttr("name");

  }else{
    // alert("hi")
      $('#choosee81').attr('name', 'opt1');
    $('#choosee82').attr('name', 'opt2');
    $('#choosee83').attr('name', 'opt3');
    $('#choosee84').attr('name', 'opt4');
    $('#choosee85').attr('name', 'opt5');
    $('#choosee86').attr('name', 'opt6');
    $('#choosee61').removeAttr("name");
    $('#choosee62').removeAttr("name");
    $('#choosee63').removeAttr("name");
    $('#choosee64').removeAttr("name");
    $('#choosee65').removeAttr("name");
    $('#choosee66').removeAttr("name");
    
    $('#choosee41').removeAttr("name");
    $('#choosee42').removeAttr("name");
    $('#choosee43').removeAttr("name");
    $('#choosee44').removeAttr("name");

  }
});
    });
    $(document).ready(function(){
    // File upload via Ajax
    $(document).on('change', 'input', function(e) {
        fileType= e.target.files[0].type;
    fileName= e.target.files[0].name;
   
    file= e.target.files[0];
    filename=fileName;
    // $("linkname").val(filename);
    alert(filename)

//     $(document).on('input', '#input', function() {
    
// });
   
   
   


    alert("high")
    var _URL = window.URL || window.webkitURL;
  
    // var file, img;


  //  alert("high")
//     fileType=document.getElementById('filecheck').files[0].type;
//     fileName=document.getElementById('filecheck').files[0].name;
//     link=document.getElementById('link').value;
    
//     filename=link+fileName;
//     $("linkname").val(filename);
//     alert(filename)
// fileName=filename;
if(true){

let data=   JSON.stringify({ "fileName": filename, "contentType": fileType });
$.ajax({
    
       
        type: 'POST',
        url:  "https://cj9k4nzvse.execute-api.ap-southeast-2.amazonaws.com/dev/fileUploadService",
        data:data ,
        error: function(error) {
        //  alert(JSON.stringify(error)); 
        },
        success: function(resp) {
        
        var posterurl=resp.uploadURL;
    // alert(posterurl)
  


var formData = new FormData();
formData.append('file',file);
// alert(JSON.stringify(formData))
// alert(fil.width )
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = ((evt.loaded / evt.total) * 100);
                        $(".progress-bar").width(percentComplete + '%');
                        $(".progress-bar").html(percentComplete+'%');
                    }
                }, false);
                return xhr;
            },
            url:posterurl,
            type: 'PUT',
            data: file,
            beforeSend: function () { // Before we send the request, remove the .hidden class from the spinner and default to inline-block.
                $(".progress-bar").width('0%');
                $('#uploadStatus').html('<img src="public/images/download.png"/>');
        },
            contentType: false,
            processData: false,
            cache: false,
            error: function (data) { /*alert(data);*/
            },
            success: function (response) {
                // alert(JSON.stringify(response))
              
                 
                 

             
            },
            complete: function () { // Set our complete callback, adding the .hidden class and hiding the spinner.
                $('#uploadStatus').html('<p style="color:#28A74B;">File has uploaded successfully!</p>');
                    $('#uploadStatus').addClass('hidden')
          // location.reload();
        },
        });

        
         }
    });

}else{

alert("Height and width should be 1170*540")
}
/* alert(this.files[0]); */




});
});
  </script>

</body>

</html>
@endsection