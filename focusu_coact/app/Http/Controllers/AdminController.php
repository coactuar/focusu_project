<?php
   
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Models\Question;  
use App\Models\Answers; 
use App\Models\Scoretable; 
use App\Models\Downloaddata;
use App\Models\User; 
use Storage;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    //  
     
    $link='';
        if($request->q_type=="option7"){
            $q_type="text";
        }else if($request->q_type=="option2"){
            $q_type="polls";
        }else if($request->q_type=="option4"){
            $q_type="link";
            $link=$original_filename = $request->files->get('link')->getClientOriginalName();
        }else if($request->q_type=="option6"){
            $q_type="file";
        }
    //  print_r($request->curans); die();
        Question::create([
            'name' =>$request->name ,
            'topic' => $request->topic,
            'date' => $request->date,
            'question' =>$request->question ,
            'q_type' => $q_type,
            'opt1' =>$request->opt1,
            'opt2' => $request->opt2,
            'opt3' => $request->opt3,
            'opt4' => $request->opt4,
            'opt5' => $request->opt5,
            'opt6' => $request->opt6,
            'opt7' =>$request->opt7,
            'opt8' =>$request->opt8,
            'curans' =>$request->curans,
            'link' =>$link ,
            'round' =>$request->round,
            'active' => '1',
            'point' =>$request->score,
           
        ]);
        // $questions = Question::all();
        // print_r($questions);
        // return view("home", ['questions'=>$questions]);
        return redirect('/admin/home')->with('status', 'Profile updated!');
    }
    public function testresult(Request $request)
    {
        $data = $request->except('_token');
        $questions=Question::where('active', 1)
       
        
        ->get();
        foreach($questions as $question){
//  print_r($request->all()); die();
        foreach ($data as $key => $value) {
        //  print_r ($key.'-'.$value);
        //    $filecheck="file".$key;
       
        
            // if($question->point==$question)
        
        
           $filename="file".$key;
        //    $data->>except('_token');
        //  
            $filetype="other";
           if($request->has($filename)){
            $original_filename = $request->files->get($filename)->getClientOriginalName();
            $original_filename=$request->$key.$original_filename;
            // print_r($original_filename);
            $data[$key]=$original_filename;
            // print_r($filename);
            $filetype="link";
            unset($data[$filename]);
            unset($data[$filename]);
         
            }
              
            $id = auth()->user()->id;
            $point=$question->point;
            if($question->id==$key){
                if($question->q_type=='polls'  ){
                    // print_r($value);
                if($question->curans==$value){
                   
                    $point=$question->point;
                }else{
                    $point='0';
                }
            }
            Answers::updateOrCreate(
                [
                    'userid' =>$id ,
                    'qid' =>$key ,
                ],
                [
                   
                    'qtype' =>$filetype,
                    'ans' => $value,
                   
                    'points' =>$point,
                ]); 
                

            
           }}
           
        //    print_r($value);
        }
        $round1 =  Answers::join('users', 'answers.userid', '=', 'users.id')
                 ->join('questions', 'answers.qid', '=', 'questions.id')
                 ->selectRaw('answers.userid, sum(answers.points) as sum')
                 ->where('round', 'round1')->groupby('answers.userid')->get();
                
        $round2 =  Answers::join('users', 'answers.userid', '=', 'users.id')
                ->join('questions', 'answers.qid', '=', 'questions.id')
                ->selectRaw('answers.userid, sum(answers.points) as sum')
                ->where('round', 'round2')->groupby('answers.userid')
                ->get();
        $round3 =  Answers::join('users', 'answers.userid', '=', 'users.id')
                ->join('questions', 'answers.qid', '=', 'questions.id')
                ->selectRaw('answers.userid, sum(answers.points) as sum')
                ->where('round', 'round3')->groupby('answers.userid')
                ->get();
            //    print_r($round3[0]['sum']); die();
        // ->select(['answers.userid','questions.question','questions.round','answers.qid','users.name', 'users.email','answers.id', 'answers.ans','answers.created_at','answers.points','qtype'])
        Scoretable::updateOrCreate(
            [
                'userid' =>$id 
            ],
            [
                'round1' =>isset($round1[0]['sum']) ? $round1[0]['sum'] : '0',
                'round2' => isset($round2[0]['sum']) ? $round2[0]['sum'] : '0',
                'round3'=>isset($round3[0]['sum']) ? $round3[0]['sum'] : '0'
            ]);    
      
      
      
        // Scoretable::create([
        //     'userid' =>$id ,
        //     'round1' =>isset($round1[0]['sum']) ? $round1[0]['sum'] : '0',
        //     'round2' => isset($round2[0]['sum']) ? $round2[0]['sum'] : '0',
        //     'round3'=>isset($round2[0]['sum']) ? $round2[0]['sum'] : '0',
          
           
           
        // ]);
        
        return redirect('/home')->with('status', 'Profile updated!'); 
   
    }
    public function updatescore(Request $request)
    {
        $id = $request->id;
        
        $data=$request->except('id','_token');
        // print_r($data); die();
        foreach ($data  as $key => $value) {
            Answers::updateOrCreate(
                [
                   'userid'   => $id,
                   'qid' =>$key
                ],
                [
                   'points'     => $value,
                  
                ]);


        }
        $round1 =  Answers::join('users', 'answers.userid', '=', 'users.id')
        ->join('questions', 'answers.qid', '=', 'questions.id')
        ->selectRaw('answers.userid, sum(answers.points) as sum')
        ->where('round', 'round1')->where('userid', $id)->groupby('answers.userid')->get();
       
    $round2 =  Answers::join('users', 'answers.userid', '=', 'users.id')
       ->join('questions', 'answers.qid', '=', 'questions.id')
       ->selectRaw('answers.userid, sum(answers.points) as sum')
       ->where('round', 'round2')->where('userid', $id)->groupby('answers.userid')
       ->get();
    $round3 =  Answers::join('users', 'answers.userid', '=', 'users.id')
       ->join('questions', 'answers.qid', '=', 'questions.id')
       ->selectRaw('answers.userid, sum(answers.points) as sum')
       ->where('round', 'round3')->where('userid', $id)->groupby('answers.userid')
       ->get();
        Scoretable::updateOrCreate(
            [
               'userid'   => $id
              
            ],
            [
            'round1' =>isset($round1[0]['sum']) ? $round1[0]['sum'] : '0',
            'round2' => isset($round2[0]['sum']) ? $round2[0]['sum'] : '0',
            'round3'=>isset($round3[0]['sum']) ? $round3[0]['sum'] : '0',
              
            ]);
            return redirect('/score-card')->with('status', 'Profile updated!'); 
    }
  
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
        return view('adminHome');
    }
   
       public function downloaddata(Request $request)
       {
        $fileName=$request->vidId;
    
$data=number_format(Storage::disk('s3')->size($fileName) / 1048576,2);
$size=Downloaddata::select('*')->first();
$data=$size['datadownload']+$data;

Downloaddata::updateOrCreate(
    [
       'id'   =>'1'
      
    ],
    [
    'datadownload' =>isset($data) ? $data : '0',
   
      
    ]);
return response()->json($data);
       }
}