<?php

namespace App\Exports;

use App\Models\Scoretable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class Export implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = Scoretable::raw('SUM(round1+round2+round3) AS total');  
    $answers = Scoretable::join('users', 'scoretables.userid', '=', 'users.id')
            // ->join('questions', 'scoretables.qid', '=', 'scoretables.id')
           
            ->select(['scoretables.userid','users.name', 'users.email','scoretables.round1', 'scoretables.round2','scoretables.round3' ])->get();
            $i=0;
            foreach($answers as $answer ){
             
               $sum=$answer['round1']+$answer['round2']+$answer['round3'];
           
               $answers[$i]['total']=$sum;
               $i++;

           }
        //    print_r($answers); die();
        return $answers;
    }
    public function headings(): array
    {
        return [
            'id',
            'name',
            'name',
            'round1',
            'round2',
            'round3',
            'total',
        ];
    }
}
