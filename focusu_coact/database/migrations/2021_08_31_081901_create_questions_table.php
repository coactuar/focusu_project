<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('topic')->nullable();
            $table->string('date')->nullable();
            $table->string('question')->nullable();
            $table->string('q_type')->nullable();
            $table->string('opt1')->nullable();
            $table->string('opt2')->nullable();
            $table->string('opt3')->nullable();
            $table->string('opt4')->nullable();
            $table->string('opt5')->nullable();
            $table->string('opt6')->nullable();
            $table->string('opt7')->nullable();
            $table->string('opt8')->nullable();
            $table->string('curans')->nullable();
            $table->string('round')->nullable();
            $table->string('active')->nullable();
            $table->string('point')->nullable();
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
